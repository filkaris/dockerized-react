# source https://mherman.org/blog/dockerizing-a-react-app/
FROM node:12.7.0-alpine
WORKDIR /app
COPY ./package.json ./package.json
RUN npm install
CMD ["npm", "start"]

